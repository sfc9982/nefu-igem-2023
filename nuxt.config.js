const webpack = require('webpack');
const {CI_PAGES_URL} = process.env
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

export default {
    // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
    ssr: false,
    router: {
        base: base,
        extendRoutes(routes, resolve) {
            routes.push({
                name: 'custom',
                path: '*',
                component: resolve(__dirname, 'pages/index.vue')
            })
        }
    },
    // Target: https://go.nuxtjs.dev/config-target
    target: 'static',

    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        script: [
            // { src: '/js/Mathjax/MathJax.js?config=TeX-AMS_HTML'},
            // {src: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/MathJax.js?config=TeX-MML-AM_CHTML'}
        ],
        title: '',
        htmlAttrs: {
            lang: 'en'
        },
        base: {
            href: 'router.base'
        },
        titleTemplate: '%s  ' + 'iGEM NEFU_China 2023',
        meta: [
            {
                charset: 'utf-8'
            },
            {
                name: 'viewport', content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description', name: 'description', content: 'This is iGEM NEFU_China 2023 team wiki' || ''
            },
            {
                hid: 'keywords', name: 'keywords', content: ''
            },
            {
                hid: 'og:site_name', property: 'og:site_name', content: 'iGEM NEFU_China 2023'
            },
            {
                hid: 'og:type', property: 'og:type', content: 'website'
            },
            {
                hid: 'og:url', property: 'og:url', content: ''
            },
            {
                hid: 'og:title', property: 'og:title', content: 'iGEM NEFU_China 2023'
            },
            {
                hid: 'og:description', property: 'og:description', content: 'This is iGEM 2023 NEFU_CHina team wiki'
            },
            {
                hid: 'og:image',
                property: 'og:image',
                content: 'https://static.igem.wiki/teams/4827/wiki/logo.png'
            },
            {
                hid: 'fb:app_id', property: 'fb:app_id', content: 'App-ID'
            },
            {
                hid: 'twitter:card',
                name: 'twitter:card',
                content: 'https://static.igem.wiki/teams/4827/wiki/logo.png'
            },
            {
                hid: 'twitter:site', name: 'twitter:site', content: '@Twitter'
            }
        ],
        link: [
            {
                rel: 'icon',
                type: 'image/x-icon',
                href: 'https://static.igem.wiki/teams/4827/wiki/logo.png'
            }
        ]
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        '@/assets/css/reset.css',
        '@/assets/css/global.css'
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        {
            src: '~/plugins/vue-carousel'
        },
        {
            src: '~/plugins/mjx'
        }
    ],
    // Auto import components: https://go.nuxtjs.dev/config-components
    components: [
        {
            path: '@/components',
            pathPrefix: false
        }
    ],

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [],

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
        postcss: null,
        transpile: ['gsap'],
        vendor: ["jquery"],
        plugins: [
            new webpack.ProvidePlugin({
                $: "jquery"
            })
        ],
        extend(config, ctx) {
            // Run ESLint on save
            if (ctx.isDev && ctx.isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    // loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        }
    },
    generate: {
        dir: 'public'
    },
    server: {
        host: "0.0.0.0"
    }
}
